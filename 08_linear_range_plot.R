library(scales)


# i <- 8
# sel_cmp <- std %>% slice(i) %>% pull(compound)
# sel_rt <- std %>% slice(i) %>% pull(rt)


get_linear_cmp_info <- function(sel_cmp, sel_rt, peaklist, std){
    
    print(sel_cmp)
    
# Get selected data
sel_data <- peaklist %>%
                mutate(cmp_l = strsplit(cmp, "  OR  ")) %>%
                filter(map2_lgl(sel_cmp,cmp_l, ~..1 %in% ..2) | map2_lgl(sel_cmp,cmp_l, ~ paste0(..1, " (non-unique hit)") %in% ..2)) %>%
                # filter(abs(SampleInfo_rt-sel_rt*60)<0.1*60) %>%
                filter(min(abs(rt-sel_rt*60)) == abs(rt-sel_rt*60))

sel_data <- sel_data %>%
                mutate(calib_dil = 2^(calib-1))


# Get linear range
calib_dil_min <- std %>% 
                    filter(compound == sel_cmp) %>% 
                    pull(ifelse(unique(peaklist$ion_mode)=="POS","lin_dil_min_pos", "lin_dil_min_neg"))

calib_dil_max <- std %>% 
                    filter(compound == sel_cmp) %>% 
                    pull(ifelse(unique(peaklist$ion_mode)=="POS","lin_dil_max_pos", "lin_dil_max_neg"))


# Calculate calibration curve
sel_data_std <- sel_data %>% 
                    filter(sample_type=="std") %>% 
                    filter(calib_dil>=calib_dil_min, calib_dil<=calib_dil_max)

if(nrow(sel_data_std)==0){
return(
    data_frame(
                name = sel_cmp,
                conc_eval_start = NA,
                conc_eval_end   = NA,
                
                conc_dil_min    = NA,
                conc_dil_max    = NA,
                
                conc_5_dil      = NA,
                conc_1_dil_pro  = NA
                )
)
}


reg_curve <-  sel_data_std %>% 
                mutate(real_conc = cmp_conc / calib_dil) %>% 
                {lm(log10(SampleInfo_into)~log10(real_conc), data=.)}

cc <- coef(reg_curve)
rsq <- summary(reg_curve)$adj.r.squared


# Standard addition curve
# std_add_dil_min <- std %>% filter(compound == sel_cmp) %>% pull(wine_std_min)
# std_add_dil_max <- std %>% filter(compound == sel_cmp) %>% pull(wine_std_max)
# 
# sel_data_wine <- sel_data %>%
#                     mutate(real_conc = (cmp_conc / calib_dil)/2) %>% # devision by 2 because if the 1:1 mix with wine.
#                     filter(calib<=std_add_dil_min, calib>=std_add_dil_max) %>% 
#                     filter(sample_type=="wine spiked")
# 
# noise <- sel_data %>% filter(sample_type == "std", calib>=14) %>% pull(SampleInfo_into) %>% median
# sel_data_wine_noiserem <- sel_data_wine %>% mutate(SampleInfo_into = SampleInfo_into-noise)
# 
# if(nrow(sel_data_wine)>0){
# std_addition_reg <- sel_data_wine_noiserem %>% 
#     {lm(formula =   SampleInfo_into~poly(real_conc, 2), data=.)}
# 
# std_addition_cc <- coef(std_addition_reg)
# std_addition_rsq <- summary(std_addition_reg)$adj.r.squared
# 
# std_add_conc <- (-(-std_addition_cc[2]+sqrt(std_addition_cc[2]^2-4*std_addition_cc[1]*std_addition_cc[3]))/(2*std_addition_cc[3]))*2*25
# std_add_conc <- unname(std_add_conc)
# }else{
# std_add_conc <- NA
# std_addition_rsq <- NA
# }


wine_int <- sel_data %>% 
                filter(sample_type=="wine", dilution==5) %>%
                mutate(SampleInfo_into = ifelse((SampleInfo_is_filled==0) | (SampleInfo_maxo>5e3),SampleInfo_into, NA)) %>% 
                pull(SampleInfo_into)
    
    
# Put results together
data_frame(
            name = sel_cmp,
            conc_eval_start = sel_data %>% filter(sample_type=="std") %>% filter(min(calib_dil)==calib_dil) %>% pull(SampleInfo_into) %>% {10^((log10(.)-cc[1])/cc[2])} %>% as.numeric(),
            conc_eval_start_nominal = sel_data %>% filter(sample_type=="std") %>% filter(min(calib_dil)==calib_dil) %>% mutate(nominal_conc = cmp_conc/calib_dil) %>% pull(nominal_conc),
            conc_eval_end   = sel_data %>% filter(sample_type=="std") %>% filter(max(calib_dil)==calib_dil) %>% pull(SampleInfo_into) %>% {10^((log10(.)-cc[1])/cc[2])} %>% as.numeric(),
            conc_eval_end_nominal = sel_data %>% filter(sample_type=="std") %>% filter(max(calib_dil)==calib_dil) %>% mutate(nominal_conc = cmp_conc/calib_dil) %>% pull(nominal_conc),
            
            conc_dil_min    = sel_data %>% filter(sample_type=="std") %>% filter(calib_dil>=calib_dil_min, calib_dil<=calib_dil_max) %>% filter(max(calib_dil)==calib_dil) %>% pull(SampleInfo_into) %>% {10^((log10(.)-cc[1])/cc[2])} %>% as.numeric(),
            conc_dil_min_nominal    = sel_data %>% filter(sample_type=="std") %>% filter(calib_dil>=calib_dil_min, calib_dil<=calib_dil_max) %>% filter(max(calib_dil)==calib_dil) %>% mutate(nominal_conc = cmp_conc/calib_dil) %>% pull(nominal_conc),
            conc_dil_max    = sel_data %>% filter(sample_type=="std") %>% filter(calib_dil>=calib_dil_min, calib_dil<=calib_dil_max) %>% filter(min(calib_dil)==calib_dil) %>% pull(SampleInfo_into) %>% {10^((log10(.)-cc[1])/cc[2])} %>% as.numeric(),
            conc_dil_max_nominal    = sel_data %>% filter(sample_type=="std") %>% filter(calib_dil>=calib_dil_min, calib_dil<=calib_dil_max) %>% filter(min(calib_dil)==calib_dil) %>% mutate(nominal_conc = cmp_conc/calib_dil) %>% pull(nominal_conc),
            
            conc_5_dil      =  ifelse(wine_int==0,NA,as.numeric(10^((log10(wine_int)-cc[1])/cc[2]))),
            conc_1_dil_pro  = 5*conc_5_dil,
            cc1 = cc[1],
            cc2 = cc[2],
            rsq = rsq,
            std_add_conc = std_add_conc,
            std_addition_rsq = std_addition_rsq
            )

}






linear_cmp_info_pos <- std %>% 
                        select(compound, rt) %>% 
                        mutate(info = map2(compound, rt, ~get_linear_cmp_info(..1, ..2, peaklist_pos_long, std))) %>% 
                        unnest(info)

linear_cmp_info_neg <- std %>% 
                        select(compound, rt) %>% 
                        mutate(info = map2(compound, rt, ~get_linear_cmp_info(..1, ..2, peaklist_neg_long, std))) %>% 
                        unnest(info)


# 
# linear_cmp_info %>% 
#                     mutate(compound = factor(compound,rev(unique(compound)))) %>% 
#                     {
#                         ggplot(data = ., aes(x = conc_eval_start_nominal, y = compound, xend = conc_eval_end_nominal, yend = compound)) +
#                         geom_segment(aes(color="Response not linear"), size=2) +
#                         geom_segment(aes(x = conc_dil_min_nominal, y = compound, xend = conc_dil_max_nominal, yend = compound, color = "Response linear"), size=2) +
#                         geom_point(aes(x=conc_1_dil_pro, color="Content in wine mix"), shape=4, size=3, stroke = 2) +
#                         scale_x_log10(breaks = unique(signif(seq(0.001, max(100, na.rm=TRUE), by = 0.01),1)), labels = comma) + 
#                         theme_classic() +
#                         theme(axis.text.x = element_text(angle=90, vjust=0.5)) +
#                         labs(x = "Concentration (ppm)") +
#                         coord_cartesian(xlim = c(0.02, 50)) +
#                         scale_colour_manual(name="",values=c(`Response not linear` = "red", `Response linear` = "black", `Content in wine mix` = "blue"), guide = guide_legend(override.aes=aes(fill=NA)))
#                             
#                     }





# bind_rows(linear_cmp_info_pos, linear_cmp_info_neg, .id="ionmode") %>% 
#         mutate(ionmode = ifelse(ionmode==1,"POS", "NEG")) %>% 
#         mutate(compound = factor(compound,rev(unique(compound)))) %>% 
#     
#         {
#             ggplot(data = ., aes(x = conc_eval_start_nominal, y = compound, xend = conc_eval_end_nominal, yend = compound)) +
#             geom_segment(aes(alpha="Response not linear", color=ionmode), size=2, position = position_dodge(width = 0.2)) +
#             geom_segment(aes(x = conc_dil_min_nominal, y = compound, xend = conc_dil_max_nominal, yend = compound, alpha = "Response linear", color=ionmode), size=2, position = position_dodge(width = 0.2)) +
#             geom_point(aes(x=conc_1_dil_pro, colour="Content in wine mix"), shape=4, size=3, stroke = 2) +
#             scale_x_log10(breaks = unique(signif(seq(0.001, max(100, na.rm=TRUE), by = 0.01),1)), labels = comma) + 
#             theme_classic() +
#             theme(axis.text.x = element_text(angle=90, vjust=0.5)) +
#             labs(x = "Concentration (ppm)") +
#             # coord_cartesian(xlim = c(0.02, 50)) +
#             scale_alpha_manual(name="",values=c(`Response not linear` = 0.3, `Response linear` = 1), guide = guide_legend(override.aes=aes(fill=NA)))+
#             scale_colour_manual(name="",values=c(`Content in wine mix` = "black", POS="blue", NEG="red"), guide = guide_legend(override.aes=aes(fill=NA)))
#                 
#         }




p_dat <- bind_rows(linear_cmp_info_pos, linear_cmp_info_neg, .id="ionmode") %>% 
        mutate(ionmode = ifelse(ionmode==1,"POS", "NEG")) %>% 
        left_join(select(cmp,compound, `Compound class`), by = "compound") %>% 
        filter(!(compound=="4-Hydroxybenzoic acid")) %>% 
        mutate(`Compound class`  = gsub("Hidroxy","Hydroxy",`Compound class`)) %>% 
        mutate(compound = factor(compound,rev(unique(compound)))) %>% 
        arrange(`Compound class`) %>% 
        mutate(`Compound class` = factor(`Compound class`,unique(`Compound class`))) %>% 
        mutate(conc_eval_end_nominal = pmax(conc_eval_end_nominal,0.005))
        

is.even <- function(x){ x %% 2 == 0 }

breaks <- seq(0.001, max(100, na.rm=TRUE), by = 0.001) %>% 
    signif(1) %>% 
    # ifelse(is.even(.*1e10),.,NA) %>% 
    unique %>% 
    {.[c(TRUE, TRUE, FALSE, TRUE, FALSE, TRUE, FALSE, TRUE, FALSE)]}

p <- 
    ggplot(data = p_dat, aes(ymin = conc_eval_start_nominal, x = compound, ymax = conc_eval_end_nominal)) +
    geom_linerange(aes(alpha="Response not linear", color=ionmode), size=2, position = position_dodge(width = 0.5)) +
    geom_linerange(aes(ymin = conc_dil_min_nominal, ymax = conc_dil_max_nominal, alpha = "Response linear", color=ionmode), size=2, position = position_dodge(width = 0.5)) +
    coord_flip() +
    geom_point(aes(y=conc_1_dil_pro, color=ionmode), shape=4, size=2, stroke = 2, position = position_dodge(width = 0.5)) +
    scale_y_log10(breaks = breaks, labels = comma) + 
    theme_classic() +
    theme(axis.text.x = element_text(angle=90, vjust=0.5)) +
    labs(y = "Concentration (ppm)", x = "Compound") +
    scale_alpha_manual(name="",values=c(`Response not linear` = 0.3, `Response linear` = 1), guide = guide_legend(override.aes=aes(fill=NA))) +
    scale_colour_manual(name="",values=c(POS="blue", NEG="red"), guide = guide_legend(override.aes=aes(fill=NA))) +
    # facet_grid(`Compound class`~.,scales = "free_y", switch = "y", space="free_y") +
    facet_wrap(`Compound class`~.,scales="free_y", ncol=1) +
        theme(strip.text = element_text(face="bold", size=9),
        strip.background = element_rect(fill="lightgrey", colour="black",size=1)) + 
    theme(axis.title = element_text(size = 18, face = "bold"),
          axis.text  = element_text(size = 12)
          )


# From 'df', get the number of 'items' for each 'label'.
# That is, the number y-breaks in each panel.
N <- p_dat %>% group_by(`Compound class`) %>% summarise(n = n()) %>% pull(n)

g = ggplotGrob(p) 

# Get the items in the g1 layout corresponding to the panels.
panels1 <- g$layout$t[grepl("panel", g$layout$name)]

# Replace the default panel heights with relative heights
g$heights[panels1] <- unit(N, "null")

## Draw g1
grid.newpage()
grid.draw(g)



ggsave("plots/dynamic_range.pdf", g, width = 20, height = 14, scale=0.8)